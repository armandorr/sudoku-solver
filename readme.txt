Compile & execute c++:
	gcc -o sudoku sudoku.cc
	./sudoku

Compile & execute python:
	python sudoku.py

Usage:
	Insert a 9x9 matrix row by row by treating the blank spaces as zeros.

Example:
		
8 0 0 0 0 0 0 0 0
0 0 3 6 0 0 0 0 0
0 7 0 0 9 0 2 0 0
0 5 0 0 0 7 0 0 0
0 0 0 0 4 5 7 0 0
0 0 0 1 0 0 0 3 0
0 0 1 0 0 0 0 6 8
0 0 8 5 0 0 0 1 0
0 9 0 0 0 0 4 0 0

	or

0 5 0 0 2 0 0 0 8
3 2 6 0 0 9 0 0 0
7 0 0 0 0 0 0 0 0
4 0 0 0 3 0 0 0 9
6 3 0 5 9 0 8 7 2
0 0 7 0 0 0 3 4 5
0 6 0 8 0 0 4 0 0
0 0 0 0 0 0 6 0 0
0 0 3 0 7 6 0 8 0

	Follow the displayed instructions. Enjoy!
