import numpy as np

def check_col(matrix,j): #check col j rep
    b = []
    for k in range(9):
       b.append(False)
    for i in list(range(9)):
        a = matrix[i][j]-1
        if a != -1:
            if b[a]: return True
            b[a] = True
    
    return False

def check_row(matrix,i): #check row i rep
    b = []
    for k in range(9):
       b.append(False)
    for j in list(range(9)):
        a = matrix[i][j]-1
        if a != -1:
            if b[a]: return True
            b[a] = True
    
    return False

def check_square(matrix,i,j): #check square i,j rep
    b = []
    for h in range(9):
       b.append(False)
    i = do_square(i)
    j = do_square(j)
    for k in list(range(3)):
        for t in list(range(3)):
            a = matrix[i+k][j+t]-1
            if a != -1:
                if b[a]: return True
                b[a] = True
    return False

def do_square(i):
    if i<3: return 0
    elif i<6: return 3
    else: return 6

def sudoku(matrix,i,j):
    find = False
    while (not find) and matrix[i][j] < 9:
        correct = True
        matrix[i][j] += 1
        correct = not(check_col(matrix,j) or check_row(matrix,i) or check_square(matrix,i,j))
        if correct:
            k = i
            t = j
            while (k != 9) and (matrix[k][t] != 0):
                if t == 8:
                    k += 1
                    t = 0
                else: t += 1
            if k != 9: find = sudoku(matrix,k,t)
            else: find = True
            
        if matrix[i][j]==9 and not find:
            matrix[i][j] = 0
            return find
        
    return find
    
def main():
    matrix = np.zeros((9, 9), dtype = int)
    print("Insert a 9x9 sudoku matrix please. (Treat blank spaces as 0's)")
    i_zero = 0
    j_zero = 0
    found = False
    for i in list(range(9)): #insert matrix
        line = input()+"\n"
        j = 0
        ptr = -1
        while j < 9:
            ptr += 1
            if line[ptr] == " ":
                continue
            if int(line[ptr]) == 0 and not found:
                i_zero = i
                j_zero = j
                found = True
            matrix[i][j] = int(line[ptr])
            j += 1
            
    finish = sudoku(matrix,i_zero,j_zero)
    if finish:
        print("Si que tiene solución:")
        print(matrix)
    else:
        print("No tiene solución")
        
main()





