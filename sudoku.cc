#include <iostream>
#include <vector>

using namespace std;

typedef vector<int> FilaI;
typedef vector<FilaI> MI;

typedef vector<bool> FilaB;
typedef vector<FilaB> MB;

const int N = 9; //dimensio matriu

bool repfila(const MI& a, int b){
	//true si repite fila b
	//false si no repite fila b
	
	FilaB B(N,false);
	for (int i = 0; i < N; ++i){
		int c = a[b][i]-1;
		if (c != -1){
			if(B[c]) return true;
			B[c] = true;
		}
	}
	return false;
}

bool repcol(const MI& a, int b){ 
	//true si repite col b
	//false si no repite col b
	
	FilaB B(N,false);
	for (int i = 0; i < N; ++i){
		int c = a[i][b]-1;
		if (c != -1){
			if(B[c]) return true;
			B[c] = true;
		}
	}
	return false;
}

void dibuixa(const MI& a){ //dibuixa matriu
	for (int i = 0; i < N; ++i){
		cout << " -------------------------------------" << endl;
		for (int j = 0; j < N; ++j){
			if(a[i][j] == 0) cout << " |  ";
			else cout << " | " << a[i][j];
		}
		cout << " |" << endl;
	}
	cout << " -------------------------------------" << endl << endl;
}

bool repcuad(const MI& a, int i, int j){
	//true si repite cuadrado 3x3 empezando por i,j
	//false si no repite cuadrado 3x3 empezando por i,j
	
	FilaB B(N,false);
	for (int k = 0; k < N/3; ++k){
		for (int l = 0; l < N/3; ++l){
			int c = a[i+k][j+l]-1;
			if (c != -1){
				if(B[c]) return true;
				B[c] = true;
			}
		}
	}
	return false;
}

int cuadrados(int n){
	if (n <= 2) return 0;
	if (n <= 5) return 3;
	return 6;
}

//funcion principal recursiva que rellena por backtracking el sudoku
bool sudoku(MI& a, int i, int j){
	bool find = false;
	while (not find and a[i][j] < 9){ 
	//sigue hasta que encontremos solucion o
	//hasta que el numero llegue a 9
		a[i][j] += 1;
		
		bool correcto = true;
		int k = cuadrados(i);
		int t = cuadrados(j);
		if (repcuad(a,k,t)) correcto = false;
		else if (repcol(a,j)) correcto = false;
		else if (repfila(a,i)) correcto = false;
		
		if (correcto){
			int b = i;
			int c = j;
			while (b != 9 and a[b][c] != 0){ //buscamos el siguiente 0
				if (c == N-1){
					++b;
					c = 0;
				}
				else ++c;
			}
			//llamada recursiva para ver si tiene solucion el siguiente paso
			if (b != 9) find = sudoku(a, b, c);
				
			else find = true;	
		}
	
		if (a[i][j] == 9 and not find){ 
		/*
		si el numero es 9 y no se encontro la solucion
		volvemos a ponerlo a 0 y retornamos false porque 
		con el anterior no hay solucion
		*/
			a[i][j] = 0;
			return find;
		}
	}
	return find;	
}

void searchFirstZero(const MI& a, int& ii, int& jj){
	for(int i = 0; i < N; ++i){
		for(int j = 0; j < N; ++j){
			if(a[i][j] == 0){
				ii = i;
				jj = j;
				return;
			}
		}
	}
}

int main(){
	//inicializamos y llenamos el sudoku
	MI a(N, FilaI(N));
	int ii, jj;
	bool first = true;
	
	for (int i = 1; i <= N; ++i){
		cout << "Please insert the line number " << i << endl;
		for (int j = 0; j < N; ++j){
			cin >> a[i-1][j];
			if(a[i-1][j] == 0 and first){ 
				ii = i-1; 
				jj = j;
				first = false;
			}
		}
	}
	char c = 'n';
	while(c == 'n'){
		
		cout << "Is your sudoku input correct? [y,n]" << endl;
		dibuixa(a);
		cin >> c;
		
		if (c == 'n'){
			int i, j;
			cout << "Wich row has a mistake? [1..9]" << endl;
			cin >> i;
			cout << "Wich column has a mistake? [1..9]" << endl;
			cin >> j;
			cout << "Wich is the correct number at position [" << i << "," << j << "] ?" << endl;
			cin >> a[i-1][j-1];
			//search first zero because it may have changed
			searchFirstZero(a,ii,jj);
		}
	}
	
	bool okay = sudoku(a,ii,jj); //llamamos a la funcion en el primer 0
    
    
	if (okay){
		cout << endl << "It has a solution and one of them is this one: " << endl << endl;
		dibuixa(a); //dibuja la matriz si tiene solucion
	}
	
	else cout << endl << "It didn't have solution" << endl << endl;
}
